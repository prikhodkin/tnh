const menu = defineEventHandler(() => {
    return {
        data: [
            {
                id: 1,
                name: 'Dashboard',
                icon: 'Squares2X2',
                to: {
                    name: 'dashboard',
                },
            },
        ],
    };
});

export default menu;
