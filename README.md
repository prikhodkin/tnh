# WMT CMS Dashboard

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Copy `.env.example` to `.env` and configure with your local development environment.

Make sure to install the dependencies:

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install --shamefully-hoist
```

## Development Server

Start the development server on http://localhost:3000

```bash
npm run dev
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.


## Writing tests

### Functional tests

**Location:**: ``./tests/feature/**.test.ts``

[Functional Testing](https://www.browserstack.com/guide/functional-testing)
[Nuxt Testing](https://nuxt.com/docs/getting-started/testing)

### Unit tests

**Location:** `./tests/unit/**.test.ts`

[Unit-tests](https://en.wikipedia.org/wiki/Unit_testing) should cover every component. If you changed the markup and your unit test is failing please check that you updated the snapshot of the component with running tests locally.
