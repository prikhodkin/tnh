import { IApi } from '~/api';

declare module 'vuex/types/index' {
    interface Store {
        $api: IApi;
    }
}

declare module '@nuxt/types' {
    interface Context {
        $api: IApi;
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $api: IApi;
    }
}
