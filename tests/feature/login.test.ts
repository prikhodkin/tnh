import { fileURLToPath } from 'node:url';
import { isWindows } from 'std-env';
import { describe, expect, it } from 'vitest';
import { $fetch, setup } from '@nuxt/test-utils';

await setup({
    rootDir: fileURLToPath(new URL('../', import.meta.url)),
    server: true,
    browser: true,
    setupTimeout: (isWindows ? 240 : 120) * 1000,
});

describe('login page', () => {
    it('should open the login page', async () => {
        expect(await $fetch('/login')).toContain('Sign in with');
    });

    it('should redirect to login page for non-guest pages', async () => {
        const html = await $fetch('/dashboard');

        expect(html).toContain('<title>Login</title>');
    });
});
