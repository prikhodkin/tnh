import { describe, expect, it } from 'vitest';
import DefaultButton from '~/components/Buttons/DefaultButton.vue';
import ButtonLink from '~/components/Buttons/ButtonLink.vue';
import DangerButton from '~/components/Buttons/DangerButton.vue';
import LoadingButton from '~/components/Buttons/LoadingButton.vue';
import { mount } from '@vue/test-utils';

describe('buttons', () => {
    it('default button render correctly', () => {
        expect(DefaultButton).toBeTruthy();

        const wrapper = mount(DefaultButton, {
            slots: {
                default: 'Test Button',
            },
        });

        expect(wrapper.text()).toContain('Test Button');
    });

    it('button link render correctly', () => {
        expect(ButtonLink).toBeTruthy();

        const wrapper = mount(ButtonLink, {
            slots: {
                default: 'Test Button',
            },
        });

        expect(wrapper.text()).toContain('Test Button');
    });

    it('danger button render correctly', () => {
        expect(DangerButton).toBeTruthy();

        const wrapper = mount(DangerButton, {
            slots: {
                default: 'Test Button',
            },
        });

        expect(wrapper.text()).toContain('Test Button');
    });

    it('loading button render correctly', async () => {
        expect(LoadingButton).toBeTruthy();

        const wrapper = mount(LoadingButton, {
            slots: {
                default: 'Test Button',
            },
        });

        expect(wrapper.text()).toContain('Test Button');

        await wrapper.setProps({ loading: true });

        expect(wrapper.html()).toMatchSnapshot();
    });
});
