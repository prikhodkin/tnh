import { defineConfig } from 'vite';
import { isWindows } from 'std-env';
import vue from '@vitejs/plugin-vue';
import path from 'path';

// @ts-ignore
export default defineConfig({
    resolve: {
        alias: {
            '~': path.resolve(__dirname),
        },
    },
    plugins: [vue()],
    esbuild: {
        tsconfigRaw: '{}',
    },
    test: {
        globals: true,
        testTimeout: isWindows ? 60000 : 10000,
    },
});
