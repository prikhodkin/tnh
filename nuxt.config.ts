import eslintPlugin from 'vite-plugin-eslint';
import svgLoader from 'vite-svg-loader';

const isDev = process.env.NODE_ENV === 'development';
const apiUrl = process.env.API_URL;

// https://nuxt.com/docs/getting-started/configuration
export default defineNuxtConfig({
    telemetry: false,

    build: {},

    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },

    debug: !!process.env.APP_DEBUG,

    app: {
        head: {
            meta: [{ name: 'viewport', content: 'width=device-width, initial-scale=1' }],
            script: [],
            link: [],
            style: [],
            noscript: [{ children: 'Javascript is required.' }],
            viewport: 'width=device-width, initial-scale=1, maximum-scale=1',
            charset: 'utf-8',
        },
    },

    css: ['~/assets/css/main.scss'],

    modules: ['@pinia/nuxt', 'nuxt-proxy'],

    runtimeConfig: {
        public: {
            themeName: process.env.THEME_NAME || 'default',
            apiUrl: isDev ? '/' : apiUrl,
            pusherAppKey: process.env.PUSHER_APP_KEY || undefined,
            websocketHost: process.env.PUSHER_HOST || 'localhost',
            websocketPort: process.env.PUSHER_PORT || '6001',
        },
    },

    serverHandlers: [{ route: '/mock-api/**:name', handler: '~/server/mock-api.ts' }],

    vite: {
        plugins: [
            eslintPlugin(),
            svgLoader({
                defaultImport: 'url',
            }),
            {
                name: 'hot-reload',
                handleHotUpdate({ file, server }) {
                    if (isDev && (file.endsWith('.vue') || file.endsWith('.ts'))) {
                        server.ws.send({
                            type: 'full-reload',
                            path: '*',
                        });
                    }
                },
            },
        ],
        logLevel: 'info',
    },
});
